# Meddelanden för it-25

Olika små saker som kan komma upp under lektioner.

# Demo koden från dagens lektion
__2025-01-07__

- [Zip arkiv till koden](https://htsit.se/f/mFormula.zip)

# Demo koden för verktyg till kv databasen
__2025-02-03__

- [Zip arkiv till koden](https://htsit.se/f/ada.zip)

# Demo för mimer-render 
__2025-02-24__

Ett program som bygger på samma principer som våra sajter, men använder lite andra principer.

- [Zip arkiv till koden](https://htsit.se/f/mimer-render-ultima.zip)

# Lösningen av uppgifter baserad på ada programmet
__2025-02-24__

Du kan jämföra mina lösningar på uppgiften med ditt eget.

- [Länk till lösnings dokumentet](https://docs.google.com/document/d/1It10ffNyFN6wzkefkFCZBaH6LqxYH_9_wc4wEFtgY7k/edit?usp=sharing)

# Exemmpel med konto på Classbaserat OO
__2025-02-24__

[Länk till flems](https://flems.io/#0=N4IgtglgJlA2CmIBcB2AHAOgIwBYA0IAzvAgMYAu8UyIG5hIBAhgK7kD2ASie09UgDMmsYgQEQEDJAG1QAOyZhESWvUYhS7OZW01SsJoUIACAIKlNLbceAAdOceMKlSY4XIAnCHIDmAbntHaFc5FjAAI3gPAIdjADdhFngQsMiPYwBeYwAGGMcoeHDyV3D2dgQmOTzjciYfQld3L19pAF1M4zbq9yZyCFJjS2ss3MC3Wr6B2Ah3JHMh8jaOrrGxzTkmlgp2DwAKZ2Sm7x88Y2DQiKjTgqKkUvL4So6hEXgAShsxx3IACxmMA4dA7Vb5-QgYaAdaAgmpgjA3cgdBEw+bsKzkDALADUWJRFjR2gw03cGAADixCD9dr8Zm9qgBfVaxPhQAAqdWpdSQR18HzssVB-1q9TJFKpwrpY0ZsTW7DAkHIu0IaI8pGSqPRpwSsCSSAuaT5XzcKrVGG1SQ6ypYqvgZsS8GMAFp4vaYTTweaHVl3XadQ6sS6-QymY5Sc1FYaBYMtMqEET2D5qXCOABlTzHXZvU62DTlHZ3IPGARaciOgDu8AgPh+xWMpVgUBz2dzsHz4QMpAA1n4iyXy5Xq7X642QJLYtLHGNU+nfJnPlH1u5jB54IQWLBEVkAAYAUlIABJgD6DvTXHvDz7PfSt0aV+RrQ4V2uN8GZbEepNjGHvORTLBYHO-KOI4i4PPGiY5nuGraIQTbGDmlAAB6lgUmgeL0EBaK4VgFB40xyPAfiaK2HhID4K7wFUvbaP2VY1iU5RQD2xH5uR8CUTmY7Ab2eyLoiTD4uixjsAIZiCYSxLkJG3GOAJCxkuGmYwhOxjStK9h8YMTCkhAtSwEC8BlmJCy7DmADC2m6cIcFYNk2RYKcnhJGOpCWXpGAsuyEEgGZcpgFonExJpK7DE4hnGQSio5tw2hwQATHZWBjiFGKeRyOYADIQHExyBfYKWYn5um7K5Ol6acABsdnZGO0EYt+2h-gBdLqMQZB9DGNAAMwVUgtlYCA9KtPSQA)
