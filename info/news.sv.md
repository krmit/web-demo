# Nyheter

Diverse nyheter om vad som sker. 

# Deepseek
__2024-01-27__

Det har blivit allmänt kännt att Deepseek har kommit ut med en LLM som 
kallas r1 som är ungefär lika bra som motsvarande model från OpenAI men 
den är helt grattis och använder mycket mindre resurser.


# Meta slutar med att faktchecka
__2025-01-07__

Meta har tidigare haft mänskliga experter som har kontrollerat vad som skrivs på deras sajter, men det komemr man sluta med.

- [Slaschdot](https://tech.slashdot.org/story/25/01/07/189242/meta-ends-fact-checking-on-facebook-instagram-in-free-speech-pitch)

# Coffezilla sågar Valv
__2024-12-18__

Den kända youtube journalisten Coffezilla har undersökt olika form av indirekta spel om pengar inom dataspelsindustion.

- [Youtube](https://www.youtube.com/watch?v=q58dLWjRTBE)
