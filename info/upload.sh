#!/usr/bin/env bash
set +e;
ghmdd ./news.sv.md
scp news.sv.html krm@htsit.se:~/www/n/index.html
ghmdd ./activity.sv.md
scp activity.sv.html krm@htsit.se:~/www/x/index.html
ghmdd ./msg.sv.md
scp msg.sv.html krm@htsit.se:~/www/m/index.html
ghmdd ./msg-it-26.sv.md
scp msg-it-26.sv.html krm@htsit.se:~/www/m/it-26/index.html
ghmdd ./msg-it-25.sv.md
scp msg-it-25.sv.html krm@htsit.se:~/www/m/it-25/index.html
