# Meddelanden för it-26

Olika små saker som kan komma upp under lektioner.

# Om att avrunda tal
__2025-01-14__

- [editor](https://flems.io/#0=N4IgZglgNgpgziAXAbVAOwIYFsZJAOgAsAXLKEAGhAGMB7NYmBvEAXwvW10QICsEqdBk2J4wAVzTViEegAIMANwBOkgCYYAFGnFYAlHOAAdNHLmxic5fHFRLAXjk6scgFQBGAAzeA3CbNCcLSw+FC0AOaa1nC2xHp+plY2dnKOALIYxIT4YGG0ylHJcf5ygcEwoRGFMXbxJdGxqUk1xAD0XgkB9EEhYZENtZ3NjemZ2cq06tWxeiVlvVUDcUNLTUvt3vUwxOLKpksmrCYm8xV9mkqqaBqaAEz4AKx6epQgcDCw0rJoCDzuAMyIW7-NgcECYHB4fDUOACGj0RjMHhsAC6rCAA)

# Två exempel om att skapa object från lektion
__2025-02-04__

- [N18](https://flems.io/#0=N4IgtglgJlA2CmIBcAOAzAOgOwFYA0IAhgK4AuA9gEryzmFTIBmhsAzvAYxAq8gNqgAdoTCIkIDAAtSYWCAIBjcoNLwVyECAC+eISLESAVr0XLV68UsGtSAAnIAjQ7YC8t4FoDcAHUG+A7pLc8AAUpABOxPAAlO6+tgm2Vja2ANbwAJ6utgAO4eRgOaQh3iAAooK2ghkK6bBItqXRPpWJEIwh6VkuPY0gAI5Nca2JCQ7h8ISpLaNavvGJyXYAbixR2XkFRSXllauwUQ1NLQsJjoZ8XQC62ftRLXN+gsnkCBi0AOYh582+8iDsBAKUgQZS8cQoJAATm0uhAwlEGgwClYJhAVnMpA02iuWiAA)
- [Lösning till kommande H8](https://flems.io/#0=N4IgtglgJlA2CmIBcAOAzAOgOwFYA0IAhgK4AuA9gEryzmFTIBmhsAzvAYxAq8gNqgAdoTCIkIDAAtSYWCAIBjcoNLwVyECAC+eISLESAVr0XLV68UsGtSAAgBOhQVHJhbAXluNighaQjKABQAlLa2wAA6gmH28KTE9tEAsoSkkhiOzq4hANxRWlFRVja2AO7k9lAAMhAlnnwRIJI05ACEjXi2jYQARj2EHV0gjOTkg40AKoZqA-JDU72zALp5vsol9q7wNXW2DSAQ4weHc40Q50cQAG5HN6cgVyedjY8nK4Vr1uQIGLQA5oEAFIAZQA8gA5DA2ewQQR-CCMACegUyLjAoJ60z8IU6gmIsFgnQALMFgqsot5fP5lA4nGiMVjSCFwkV1nZyJiPOEtOTBCN7IEEHYIFyAAw5WwigA8tgAjOLJQBqRWhSLRYrC5zwAAeXJSaQwjFoFRRdOywQAVJtRDtSL81H80mSorYNbYAA6ERxuTzW7a1Uh8WFQHXvaIcwx8T3epZc1GuABqLGI8FyXUEBUEUVi8UStgjq0zHwpPj8AWi8bASdgKeZarCrrZtnYCDLNM8+vSRtGAsrIQt8uCLrCw+bpQgpAUkkCLfgbcEqtHYQUhHYtlFSCXrtX8Dlm+iDe3a4ATPvD0fd2gz+fl03gzq9aku8be2awP3ypVbfa4U7VjeYjiBJok-aoAyDLVtTDADlx3WxTy3Fc1yJa8GyQ3ccFQ88c2A2ksnfUIAD51wwHB-0PdDbAANiww8cLzSsGTnJlnQPBsQ2YfFSFoht6OSJ9DRfU18P7eVRVYhtM14oC807QSe2EtFRNFcTC3kEBZ3nXhxFlTDZSJABaXSkGPHBtF0EBhFEDQMAUVgTBAKxzFIDRtCWLQgA)

# Några kommentar från lektionen.
__2025-02-05__

- [Det nya exempelet om listor](https://flems.io/#0=N4IgZglgNgpgziAXAbVAOwIYFsZJAOgAsAXLKEAGhAGMB7NYmBvEAXwvW10QICsEqdBk2J4A9GIAEAIwCeiSQCFscGAB00sYpKi1VAJziSAvJORqQAUSgQMcCxUkXFAVziqHFgLIYojCwC6ANwaGkJwtLD4ugDmABS6BnAAlCGaejCG+C5ocIQQYMRxFgDiLvr6GBapYfQRUbEJGYY1mjDaUHbapomZcPgADrQDca3hkTDRtPGdcMStWpKQht06zf15BcSbhaNp4w3TcctzqZQ0tFgD0Jl40hjSMORUqrDUxBB1eACMiAAsAAY2BwQJgcHh8NR3OchIxmDw2AFWEA)
- [Korrekt länk till Zipfs_lag](https://sv.wikipedia.org/wiki/Zipfs_lag)
- [Youtube film om Zipfs_lag](https://www.youtube.com/watch?v=fCn8zs912OE)
