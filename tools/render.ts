#!/usr/bin/env -S deno --allow-read --allow-write
import {join} from "jsr:@std/path";
import { render } from "https://deno.land/x/mustache@v0.3.0/mod.ts";
import {parse} from "jsr:@std/jsonc";

const args = Deno.args;
const folder = Deno.cwd();
console.log(args);
const template_path = join(folder, args[0]);
const data_path = join(folder, args[1]);
const destination_path = join(folder, args[2]);
console.log(`Use ${template_path} with data from  ${data_path} save it at ${destination_path}`)
const data = parse(await Deno.readTextFile(data_path));
const template = await Deno.readTextFile(template_path);

Deno.writeTextFile(destination_path, render(template, data));

