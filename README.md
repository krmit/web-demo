# NAME

## Install

If you would like to use the development toll for the site.

```
$ npm install
```

## Usage

To test a site on local computer with live update you could start a web server.

```
npm run start
```

To use opinionated code formatter to get nice format on your code.

```
npm run fmt
```

## Changelog

- **0.0.1** _2024-03-20_ First version published

## Author

**©Magnus Kronnäs 2024 [magnus.kronnas.se](magnus.kronnas.se)**
