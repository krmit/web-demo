export function input(msg:string, defultValue:string) {
		if(msg.length!==0){
		    const html_text = convert(msg);
		    print(html_text);   
	    }
		const html_text = "<input type='text'></input></br>";
		const text_input = print(html_text)[0];
		text_input.placeholder = defultValue;
		text_input.focus();

		return new Promise( function(resolve){
			text_input.onkeydown = (function(input: HTMLElement){
				return function(event:Event) {if(event.keyCode === 13)
					{
						if(input.value ==="") {
					        resolve(defultValue)
						}
				else{
					resolve(input.value);
				}}}})(input);
	});
}

export function write(...words:string[]) {
		let shortMsg = true; 
	    if(typeof words[words.length-1] === "boolean") {
			shortMsg = words.pop();
		}
		
		let html_text = convert(words);
	    playerWeb._before = playerWeb._before.then(()=> {
	    if(shortMsg) {
			html_text +="\n<button> ok!</button>";
			this._count_row=0;
			let node_list = this._append(html_text);
		    let button = node_list[node_list.length-2];
		    return new Promise( function(resolve){button.onclick = function(){return resolve(true);}});
	    }
		else {
		    this._append(html_text);
		    return Promise.resolve(true);		
		}
		});	
}

export function print(html_text:string) {
	    console.log((html_text);
		const newcontent = document.createElement('div');
		newcontent.innerHTML += html_text+"<br />";
		let result=[];
		while (newcontent.firstChild) {
			result.push(newcontent.firstChild);
		    playerWeb._term.appendChild(newcontent.firstChild);
		}
		
		result[result.length-1].scrollIntoView();
		return result;
	}

export function convert(words:string) {
	    console.log(words);
	    let html_text = "";
		for(let word of words) {
			if(typeof word === "string") {
			    html_text+=word;
			}
			else if(Array.isArray(word)) {
				html_text+=convert(word);
			}
			else {
				let classes = "";
			    if(word.color !== undefined) {
					classes += word.color + " ";
				}
				 if(word.fontWeight !== undefined) {
					classes += word.fontWeight + " ";
				}
                html_text+="<span class='"+classes+"' >" + word.msg + "</span>";
			}
	} 
	return html_text;
}

function append(htmlText:string) {
	const newcontent = document.createElement('div');
	newcontent.innerHTML += htmlText+"<br />";
	let result=[];
	while (newcontent.firstChild) {
		result.push(newcontent.firstChild);
		playerWeb._term.appendChild(newcontent.firstChild);
	}
	
	result[result.length-1].scrollIntoView();
	return result;
}	
