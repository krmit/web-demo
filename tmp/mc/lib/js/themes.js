import { getCookie } from "./cookie.js";
const style = getCookie("them") ?? "james";

if (style !== "blank") {
  const link = document.createElement("link");
  link.href = "lib/themes/" + style + "/index.css";
  link.type = "text/css";
  link.rel = "stylesheet";
  link.media = "screen,print";
  document.getElementsByTagName("head")[0].appendChild(link);
}