#!/usr/bin/env bash
USER=$1;
DIR_PATH=$HOME/skola/u/$USER;
cd $DIR_PATH;
echo -e "# $USER\n" > index.md;
echo -e "\n# Sidor\n" >> index.md;

for d in */ ; do
    TITLE=`cat $d/index.html | awk -vRS="</title>" '/<title>/{gsub(/.*<title>|\n+/,"");print;exit}'`;
    echo -e "- [$TITLE](/u/$USER/$d)" >> index.md;
done

echo -e "\n" >> index.md;
render index.md /u/$USER;

