# Ali - Animerad CSS theme

## Skapare

Jag heter Alireza Mousavi kallas också för Ali. Jag är 19 år gammal 
och går på Hässleholms tekniska skola. Jag är intresserad av 
webbutveckling och läser sista året på teknikprogrammet med 
inriktning informations- och medieteknik.

## Om themat

I det här temat har jag använt mig av CSS-gradient med två olika 
färger för bakgrunden. Jag tycker om att använda färger och ha 
olika nyanser i stället för att bara ha en färg. Därför valde jag 
att använda mig av just CSS-linear-gradient. ”Media Query” har 
också använts. Vid olika skärmstorlekar ändras också 
bakgrundsfärgen. Dessutom när sidan öppnas i mobilen d.v.s. enhet 
med skärmstorlek som har en bredd lägre än 550px visas allt på 
sidan i mitten. Detta för att information ska synas bättre.
