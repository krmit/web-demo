function bord() {
  const board_node = document.getElementById("board");
  const config = {
    orientation: "black",
    showNotation: false,
    draggable: true,
    position: "rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR b KQkq e3 0 1",
  };
  const board = Chessboard2(board_node, config);
}
