#!/usr/bin/env bash
rm -rf tmp/mc;
cp -rL mc tmp/mc;
cd tmp;
zip -r mc.zip mc
upload mc.zip
rsync -avmt -L  --include='*.html' --include='*.css'  --include='robot.txt' --include='*.png'  --include='*.gif' --include='*.js'  --include='*.json' --include='*.webp' --include='*.ttf' --include='*/' --exclude='*' ./mc/* krm@htsit.se:~/www/mc/; 
cd ..
