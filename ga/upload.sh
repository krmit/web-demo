#!/usr/bin/env bash

renderHtml index.template.html content/2024.json5 index.html
rsync -avm --include='*.html' --include='*.css' --include='*.png' --include='*.js' --include='*.webp' --include='*/' --exclude='*' ./* krm@htsit.se:~/www/ga/
