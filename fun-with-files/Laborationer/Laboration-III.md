# Laboration III

Fundera igenom relationen mellan ett skall så som bash och terminalen. 
Förklara vad de gör för något och hur de fungerar tillsammans. Skriv 
svaret längs ned.  

# Poängavdrag

- Felaktigt påstånde ger poängavdrag på uppgiften.
- Onödigt resonemang som inbte bidrar till svaret på uppgiften.
- Svårigheter att läsa svaret.

# För att få ett poäng krävs
	
- En kort beskrivning av vad en terminal gör.
- En kort beskrivning av vad bash gör.
- Beskrivningarna måste vara korrekta.

# För att få full poäng krävs

- Beskrivningarna ska vara utförliga.
- Det ska finnas en föklaring hur terminalen och bash fungerar 
tillsamans.
