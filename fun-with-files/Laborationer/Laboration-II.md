# Laboration II

Tänk på att uppgiften ska motiveras nogrant. Skriv lösningen under 
varje uppgift nedan. Alla ska göras med terminal komandon. Förklara 
alla komandon som du har använt dig av.


# Uppgifter

1. Se till att andra("other") inte kan köra eller skriva till filen 
till text filen som du skapade i uppgiften ovan, men kan läsa 
textfilen du precis skrev. Skriv också en förklaring där du 
förklara vad kommandot och  alla flagor eller annan indata gör.

2. OBS! Filen i denna uppgift finns inte, du får skriva komandona som 
om den hade funnits. Du ska inte utföra denna beskrivning, bara skriva 
ned de komandon som behövs göras. Ladda ned filen "koder.zip", 
"https://htsit.se/f/". Packa upp filen. Filen är för stor för att läsa 
igenom manuellt, men det finns en rad som börjar med "Password:", hitta 
den raden.

3. Hitta filen "king.txt" igen. Gör en symbolisk länk till mappen där 
filen "king.txt" finns.

