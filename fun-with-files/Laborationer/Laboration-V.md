# Del 1: Välkommen till vår lilla övning!

Läs instruiktionerna nedan.

- Du ska hitta olika kodord, så kallade flaggor.
- Flaggoran börjar med texten *MMITFFXX{*
- XX ovan motsvarar siffror, t.ex. XX kan vara 23.
- Sedan kodordet.
- Texten avslutas sedan med *}*
- Har du hittat en flagga så berätta inte om denna för någon annan.
- Men lämna in den på följande länk: [Formulär](https://forms.gle/mhS8dXLAfLjA3pzr8)
- Några flagor är arangerade i en slinga. Titta i [laboration VI](./Laboration-VI.html) för dessa.
- Flagorna i slingan är lätta att hitta så det kan vara en bra början.

# Om flagorna

Några kommentarer om flagorna som kan hjälpa dig hitta dem.

- Det finns 12 flagor i slingan, inklusive den som redan finns i 
flagor.txt filen.
- Det finns 2 bonus uppgifter du kan göra efter att ha gjort 
slingan. 
- Det finns förutom dessa 2 bonus uppgifter ytterligare 2 uppgifter med beskrivning 
i mappen "/uppgifter".
- Sedan i vilken ordning inlämningen har gjorts.

__Tips:__ Läs instruktionerna nogrant och du hittar förasta flaggan i 
detta dokument.
