# Laboration I

Glöm inte bort att du måste skriva ned varje kommando som du gör under
respektive steg nedan. Om inget annat anges i beskrivningen av vad du 
ska göra.

Texte MITT_NAMN ska bytas ut mot ditt namn eller alias.

Försök använda ett komando för varje uppgift nedan.

Lycka till!

1. Gå till mappen _Computer_.

2. Gå till mappen _/OS/Windows/_.

3. Lista alla filer som finns i mappen.

4. Lista alla filer i mappen "Computer" och se till att storleken på 
dem visas på ett för en människa lättförståeligt sätt.

5. Välj en av bilderna och byt namn till _MITT_NAMN-favorite.png_ . 

6. Kopiera bilden ovan till mappen _Computer/OS/Linux_.

7. Gå till mappen “Linux”.

8. Du har ångrat dig så ta bort bilden som du kopierade hit.

9. Gå till mappen Computer/People/RMS

10. Läs filen _gpl-3.0.txt_. Du ska använda ett terminalkommando för 
att se innehållet.

11. Gå till mappen som denna fil som du läser nu ligger i.

12. Skapa en mapp med samma namn som ditt alias.

13. Skapa en textfil med valfritt namn i mappen du skapade.

14. Öppna den men en terminal editor och lägg till en valfri mening och 
spara den nya texten.

15. Se till att andra("other") inte kan läsa, skriva eller köra 
textfilen du precis skrev.

16. Gör så att gruppmedlemmar till din textfilen bara kan skriva till 
filen, men inte läsa eller köra den.

17. Gör en symbolisk länk till filen som du precis har skapat som 
hetter "min_fil.txt" och ligger i denna mapp.

18. Ladda ned en valfri bild till denna mapp.

19. Byt namn på bilden till något långt och skrämmande.

20. Gå upp en nivå i filhirakin.

21. Komprimmera mappen du skapde med hjälp av zip.

22. Gå till den första mappen i fin-with-files.

23. Hitta filen "king.txt" med hjälp av kommandona find och grep.

24. Hitta filen som heter trex.jpg med hjälp av bash kommandon.
