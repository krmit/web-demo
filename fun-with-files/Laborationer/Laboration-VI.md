# Laboration VI

# Del I: Början på en resa

I denna slinga så finns det ledtrådar. Varje ledtråd leder till en 
flagga samt en ledtråd till nästa steg i slingan. Rapportera in 
flaggorna och se om du kan hitta alla flaggor i slingan.

Ta det som en utmaning att bara använda kommandon i terminalen för att 
lösa dessa flagor. 

Detta är fösta delen i slingan och första flaggan har du här:

MMITFF24{Du har valt rätt, alla andra kommer ligga i lä}

Nästa del hittar du ovan i filen som hetter NEXT.md som är i den första 
mappen.
