#!/bin/bash

## Del III:

#  - Skriv en kommentar till varje kommando!
#  - Skriptet ska vara körbart.

# Du ska skriva ett enkelt program som loggar små medelanden.
# Ditt script ska ta en inpameter som ska vara ett medelande som ska 
# loggas. Du ska också använda variabeln MITT_NAMN som du sätter i 
# terminalen till ditt alias. 
#
# Tips: Testa med variabeln $1 om du inte vet hur du ska få indata till komandot.
#  
# 1. Låt ditt skript skriva ut "Do logs for MITT_NAMN". Där MITT_NAMN 
#    är innehållet i variabeln MITT_NAMN.
# 2. Lägg till dagens datum till filen "MITT_NAMN.txt"
# 3. Lägg till medelandet till logg filen och en ny rad.
# 4. Kommentera varje kod rad som du har skrivit.

# Lycka till!

# Din script kod nedanför här. 
