# Copyright för bilerna

Se länk brevid bilden fär vilken copyright som gäller

## Computer/People/Linus

Linus_Torvalds.jpeg: http://commons.wikimedia.org/wiki/File:Linus_Torvalds.jpeg

Linus*Torvalds*-_entering_dunk_tank_at_linux.conf.au_2004.jpg :
http://commons.wikimedia.org/wiki/File:Linus_Torvalds_-\_entering_dunk_tank_at_linux.conf.au_2004.jpg

Sv-Linus_Torvalds.ogg
http://commons.wikimedia.org/wiki/File:Sv-Linus_Torvalds.ogg

## Computer/OS/BSD/OpenBSD

Flera olika l�tar ifr�n http://www.openbsd.org/lyrics.html

## Computer/OS/Plan9

Plan9*process_management.png
http://en.wikipedia.org/wiki/File:Plan_9_from_Bell_Labs*%28process_management%29.png

Plan9bunnysmblack.jpg http://en.wikipedia.org/wiki/File:Plan9bunnysmblack.jpg

screenshot.jpg https://9p.io/plan9/screenshot.html

## Computer/OS/Contiki

Contiki-simulation.png
http://en.wikipedia.org/wiki/File:Contiki-ipv6-rpl-cooja-simulation.png

Contiki-screenshot.png http://en.wikipedia.org/wiki/File:Contiki-C64.png

Contiki-screenshot-1.png http://en.wikipedia.org/wiki/File:Contiki-avr.png

## Computer/People/RMS

Richard_Matthew_Stallman.jpeg
http://commons.wikimedia.org/wiki/File:Richard_Matthew_Stallman.jpeg

Richard_Stallman_Libreplanet_2012.jpg
http://commons.wikimedia.org/wiki/File:Richard_Stallman_Libreplanet_2012.jpg

Richard_Stallman_Talk_2009-10-09_part1.ogv
http://commons.wikimedia.org/wiki/File:Richard_Stallman_Talk_2009-10-09_part1.ogv

## Computer/People/ESR

Eric_S_Raymond.jpg: http://commons.wikimedia.org/wiki/File:Eric_S_Raymond.jpg

## Computer/People/Jeri

Jeri_Ellsworth_IMG_7310.jpg:
http://commons.wikimedia.org/wiki/File:Jeri_Ellsworth_IMG_7310.jpg

Jeri_Ellsworth_Thumb.jpg:
http://commons.wikimedia.org/wiki/File:Jeri_Ellsworth_at_Alt_Party_Thumb.jpg

CastAR.jpg
https://s3.amazonaws.com/ksr/assets/002/268/399/3ca907bf194106e4062dfd357afc624d_large.jpg?1405125706

CastAR-1.jpg
https://s3.amazonaws.com/ksr/assets/001/082/972/a38baa635924cb830c056ec141fb0866_large.jpg?1381541369

## Computer/People/AdaLovelace

Ada*Lovelace_portrait-1.jpg
http://en.wikipedia.org/wiki/File:Ada_Byron_aged_seventeen*%281832%29.jpg

Ada_Lovelace_portrait.jpg
http://en.wikipedia.org/wiki/File:Ada_Lovelace_portrait.jpg

## Computer/People/SteveJobs

Steve_Jobs_Headshot.jpg
https://en.wikipedia.org/wiki/Steve_Jobs#mediaviewer/File:Steve_Jobs_Headshot_2010-CROP.jpg

home.jpg https://upload.wikimedia.org/wikipedia/commons/b/bd/Apple_Garage.jpg

Steve_Jobs_1.jpg
https://en.wikipedia.org/wiki/Steve_Jobs#mediaviewer/File:Stevejobs_Macworld2005.jpg

## Computer/People/Bill

Bill_gates_portrait.jpg http://commons.wikimedia.org/wiki/Bill_Gates

Bill_Gates_mugshot.png
http://commons.wikimedia.org/wiki/Category:Bill_Gates_in_1977

Pinball.jpg
http://en.wikipedia.org/wiki/File:Visible_Pinball_III_-_Pacific_Pinball_Museum_cropped.jpg

## Computer/People/GabeNewell

Gabe_Newell_GDC_2010.jpg
https://commons.wikimedia.org/wiki/File:Gabe_Newell_GDC_2010.jpg

## Computer/OS/Mac

Macintosh_classic.jpg
http://commons.wikimedia.org/wiki/File:Macintosh_classic.jpg

IMac_G4_sunflower7.png
http://commons.wikimedia.org/wiki/File:IMac_G4_sunflower7.png

## Computer/OS/Linux

NewTux.svg http://commons.wikimedia.org/wiki/File:NewTux.svg

KNOPPIX_booting.png http://commons.wikimedia.org/wiki/File:KNOPPIX_booting.png

## Computer/OS/Linux/BSD

NetBSD_logo.png http://commons.wikimedia.org/wiki/File:NetBSD_logo.png

Screenshoot of NetBSD
https://upload.wikimedia.org/wikipedia/commons/d/d4/NetBSD.png

OpenBSD_logo.gif http://commons.wikimedia.org/wiki/File:Puflogv1000X650.gif

openbsd-pr.png https://commons.wikimedia.org/wiki/File:Openbsd37withjwm.png

## Computer/OS/Windows

Windows_family.png http://commons.wikimedia.org/wiki/File:Windows_family.svg

Windows_architecture.png
http://commons.wikimedia.org/wiki/File:Windows_95_architecture.png

bsd.svg
http://commons.wikimedia.org/wiki/File:Windows_2000_Blue_Screen_of_Death_(INACCESSIBLE_BOOT_DEVICE).svg

## Computer/games

undertale-battle.png https://commons.wikimedia.org/wiki/File:Battle.png

undertale-logo.jpg https://commons.wikimedia.org/wiki/File:Undertale-logo.jpg
!Obs unclear copyright

## Animals/Vertebrate/Birds/Hawks

Hawk.jpg http://en.wikipedia.org/wiki/File:Accipiter_striatusDO1908P02CA.JPG

## Animals/Vertebrate/Reptiles/Turtle

TurtleOmaha.jpg http://en.wikipedia.org/wiki/File:TurtleOmaha.jpg

## Animals/Vertebrate/Mammals/Bovid/Antilope

Black_Buck.jpg http://en.wikipedia.org/wiki/File:Black_Buck.jpg

## Animals/Vertebrate/Mammals/Primates/NewWorldMonkey/Lemurs

Lemur_catta.jpg http://en.wikipedia.org/wiki/File:Lemur_catta_01.jpg

Animals/Vertebrate/Mammals/Primates/NewWorldMonkey/Lemurs/Gibbon
Hylobates_lar_pair_of_white_and_black

## Animals/Vertebrate/Mammals/Primates/NewWorldMonkey/SquirrelMonkey

SaimiriSciureus.jpg
http://en.wikipedia.org/wiki/File:Saimiri_sciureus-1_Luc_Viatour.jpg

Animals/Vertebrate/Mammals/Primates/NewWorldMonkey/OldWorldMonkey/LongNosedMonkey
proboscis_monkey.jpg

## Animals/Vertebrate/Mammals/Primates/Hominidae/Orangutan

Orangutan.jpg
http://en.wikipedia.org/wiki/Orangutan#mediaviewer/File:Orang_Utan,_Semenggok_Forest_Reserve,_Sarawak,_Borneo,_Malaysia.JPG

Orangutan-1.jpg
http://en.wikipedia.org/wiki/Orangutan#mediaviewer/File:Orangutan_-Zoologischer_Garten_Berlin-8a.jpg

## Animals/Vertebrate/Mammals/Primates/Hominidae/Animals/Vertebrate/Mammals/Primates/Hominidae//Homininae/Gorillas

Gorillas.jpg https://en.wikipedia.org/wiki/File:Male_gorilla_in_SF_zoo.jpg

Gorillas-1.jpg
https://en.wikipedia.org/wiki/File:Nshongi_Gorilla_Group-7,_by_Justin_Norton.jpg

Gorillas-2.jpg https://en.wikipedia.org/wiki/File:Gorillas-moving.jpg

Gorillas-3.jpg https://en.wikipedia.org/wiki/File:Gorilla_gorilla11.jpg

## Animals/Vertebrate/Mammals/Primates/Hominidae/Animals/Vertebrate/Mammals/Primates/Hominidae/Homininae/Bonobo

Bonobo.jpg https://en.wikipedia.org/wiki/File:Bonobo_0155.jpg

Bonobo-1.jpg https://en.wikipedia.org/wiki/File:Bonobo_group_hug.jpg

Bonobo-2.jpg https://en.wikipedia.org/wiki/File:Pan_paniscus11.jpg

Bonobo-3.jpg
https://en.wikipedia.org/wiki/File:Bonobo_%28Pan_paniscus%29_at_Lola_Ya_Bonobo_-_3.JPG

Bonobo-4.jpg
https://en.wikipedia.org/wiki/File:A_Bonobo_at_the_San_Diego_Zoo_%22fishing%22_for_termites.jpg

Bonobo-5.jpg https://en.wikipedia.org/wiki/File:Pan_paniscus_%28female%29.jpg

## Animals/Vertebrate/Mammals/Primates/Hominidae/Animals/Vertebrate/Mammals/Primates/Hominidae/Homininae/chimpanzee

chimps.jpg https://en.wikipedia.org/wiki/File:Adult_male_chimps_in_mahale.jpg

Animals/Vertebrate/Mammals/Primates/Hominidae/Animals/Vertebrate/Mammals/Primates/Hominidae/Homininae/Human

Linus.jpg
https://en.wikipedia.org/wiki/File:LinuxCon_Europe_Linus_Torvalds_03.jpg

TRex.jpg
https://sv.wikipedia.org/wiki/Tyrannosaurus#/media/File:Rjpalmer_tyrannosaurusrex_001.jpg

Perfect_Cherry_Blossom_screenshot.jpg
https://en.wikipedia.org/wiki/Touhou_Project#/media/File:Perfect_Cherry_Blossom_screenshot.jpg
