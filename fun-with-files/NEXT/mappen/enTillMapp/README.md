# Del IV: Du börjar lära dig sökvägar!

- Gå till mappen fun-with-flag.
- Det var den som den första README filen låg i.
- Denna mapp kallar vi från och med för vår root map.
- I en sökväg används "/" för vår root mapp.
- Följ sökvägen nedan.

/A/B/C/D

MMITFF23{Om du går på rätt väg kommer du snart i mål}
