# Kan du decoda base64?

Följande kod innehåller en flagga:

```base64
TU1JVEZGMjN7QmFzZTY0IMOkciBldHQgYnJhIHPDpHR0IGF0dCBnw7ZtbWEgc2FrZXIgc29tIHZhbmxpZyB0ZXh0fQ==
```

- Du behöver hitta ett program för decodning av base64
- Kopiera in koden ovan.
- Skirv in flagga i flagor.txt filen.
 

