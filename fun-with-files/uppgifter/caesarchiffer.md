# Kan du decoda ett litet caesarchiffer?

Följande chiffer innehåller en flagga:

```text
NNJUGG34|Efu lbo wbsb twäsu nfe äöa~
```

- Lös siffret ovan.
- Du kan behöva anpassa svaret till hur flaggor brukar se ut för att få 
det rätt.
- Skirv in flagga i flagor.txt filen.

En liten teknisk kommentar som du inte behöver för att lösa denna 
uppgift. Svenska bokstäver följer vi det svnska alpabetet. För siffror 
går vi från 0 till 9 samt för övriga tecken används ASCII tabellen.
