# Kan du decoda detta caesarchiffer?

Följande chiffer innehåller en flagga:

```text
thnkh{på fy ifååyl fu ipshy}
```

- Det svenska alphabetet används.
- Andra tecken krypteras inte.
- Flaggan är på formen "magda{X}", där X kan vara en mening med alphabetet och mellanslag.
