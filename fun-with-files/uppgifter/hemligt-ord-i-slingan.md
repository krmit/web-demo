# Hemligt ord som du kan hitta i slingan

Om du har följt slingan och hittat alla flagor kan du skapa en bonus flagga.

- Ta det näst sista tecknet i flaggan, dvs det som är innan "}"
- Skriv dessa tecken efter varandra så bildar de ett kodord.
- Skriv detta ord mellan "MMITFF23{" och "}" så har du skapat en ny flagga.
- Lämna in denna som du har gjort med de andra flaggorna.


