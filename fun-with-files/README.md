# 🐧Fun with Files

Detta är en samling av filer och labborationer för att göra dig bättre 
på att förstå hur en filsystem ska användas. Labborationerna som 
beskrivs nedan finns i mappen Labboration.

Om du inte vet var du ska börja kan det vara värt att bar läsa igenom 
*Laboration V* och sedan göra så långt du kan på *labboration VI*.

# Redovisning

Muntligt till läraren och där efter följ instruktioner.

# **Laboration I:** Gör alla uppgifter om Linux kommandon

Du hittar uppgifterna som du ska göra i filen "Labboration-I.md". 
Labborationern består av flera små steg som ska utföras. Observera att
du under varje steg ska skriva vilket kommando som du använder. Skriv
kommandot i terminalen och kopiera in det under varje steg.

# **Laboration II:** Gör uppgifter och kommentera

Du hittar denna del i filen "Labboration-II.md". Uppgifterna  Lös 
uppgifterna genom att skriva ned kommandona du använder. Kommentera 
också alla komandon och förklar vad de gör.


# **Laboration III:** En reflektion

Du hittar denna del i filen "Lasbboration-III.md". Här ska du skriva en 
längre reflekterande text om frågeställningen.

# **Laboration IV:** Ett script

Följ instruktionerna och skapa ett litet script som gör en liten log 
över vad du gör.

# **Laboration V:** Flagor

I dessa filer finns det massa med flagor. Hitta dem och lämna in dem 
enligt instruktionerna i denna uppgift.

# **Laboration VI:** Följ en slinga

Ett antal av flagorna är organiserade i en slinga. Följ denna slinga.

# **Laboration VII:** Förbättringar

Kom med förbättringsförslag på denna uppgift. T.ex. kan du implmentera 
en till flagga.

---

**Copyright © 2018-2024, Magnus Kronnäs**

**All rights reserved.**
