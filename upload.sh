#!/usr/bin/env bash
rsync -avmt -L  --include='*.html' --include='*.css'  --include='robot.txt' --include='*.png'  --include='*.gif' --include='*.js'  --include='*.json' --include='*.webp' --include='*.ttf' --include='*/' --exclude='*' ./htsit/* krm@htsit.se:~/www/;
