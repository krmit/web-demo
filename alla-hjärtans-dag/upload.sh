#!/usr/bin/env bash

rsync -avm --include='*.html' --include='*.css' --include='*.png' --include='*.webp' --include='*/' --exclude='*' ./* krm@htsit.se:~/www/l/
