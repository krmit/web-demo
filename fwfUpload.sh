#!/usr/bin/env bash

cd fun-with-files;
ghmd README.md;
cd ..;
7z -r a fun-with-files.zip fun-with-files
scp fun-with-files.zip htsit.se:~/www/f/
rm fun-with-files.zip;
