# Bilder på enkla kretsar
*
status: alpha
author: krm
created: 2018-09-10
*

# 🖼 Serverrum
*
name: circuits-1.5A-20kO
author: camknows@flickr.com
licence: https://creativecommons.org/licenses/by-nc-sa/2.0/
link: https://www.flickr.com/photos/camknows/4712191684
*
