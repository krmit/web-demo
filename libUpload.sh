#!/usr/bin/env bash
zip -r lib.zip lib;
scp lib.zip krm@htsit.se:~/www/f/;
rm lib.zip
rsync -avm --include='*.html' --include='*.css' --include='*.png' --include='*.js'  --include='*.json' --include='*.webp' --include='*.ttf' --include='*/' --exclude='*' ./lib/* krm@htsit.se:~/www/lib/;
rsync -avm --no-links --include='*.html' --include='*.css' --include='*.png' --include='*.js'  --include='*.json'  --include='*.webp' --include='*.ttf' --include='*/' --exclude='*' ./htsit/* krm@htsit.se:~/www/;
