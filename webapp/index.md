# Mimer webapp

En samling av några webbapp som kanske kan vara till nytta. Varje webbapp består
av en html sida. Biblioteket som används finns
[här](https://htsit.se/f/lib.zip).

# Enkla vektyg

- [Konvertera från f till c](./tools/convert-c-to-f.html)
- [🚧 Skriva ett medelande](./tools/generate-msg.html)
- [Viktiga länkar](./tools/important-link.html) Endast CSS.

# Spel

- [Gissa talet](./game/guess-number.html)
- [Multiplikations frågor](./game/multiplication.html)
- [🚧 Förenklat Nim spel](./game/simpel-nim.html)

# Praktiska saker

- [Lägg till en kaka](./demo/add-cookie.html)
- [Listar alla kakor på sajten](./demo/list-of-cookies.html)
- [🚧 Spela en låt](./demo/play-sound.html)

# Demo

- [Cirklar och en skylt](./screenshows/circle-motion-sign.html)

# Extra

- [Kvar till student](/b)
- [Kvar till seminariet](/bd)

# Mallar

- [En enkel mall](./template.html)
