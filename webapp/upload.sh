#!/usr/bin/env bash

ghmd index.md
rsync -avm --include='*.html' --include='*.css' --include='*.png' --include='*.js' --include='*.webp' --include='*.ttf' --include='*/' --exclude='*' ./ krm@htsit.se:~/www/w/;
