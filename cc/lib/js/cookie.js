//https://www.tabnine.com/academy/javascript/how-to-set-cookies-javascript/
export function setCookie(Name, Value, Days = 14) {
  let date = new Date();
  date.setTime(date.getTime() + Days * 24 * 60 * 60 * 1000);
  const expires = "expires=" + date.toUTCString();
  document.cookie = Name + "=" + Value + "; " + expires + "; path=/";
}

export function getCookie(name) {
  const decoded_cookie = decodeURIComponent(document.cookie);
  let cookies = decoded_cookie.split(';');
  for(let cookie of cookies) {
    cookie = cookie.trim();
    if (cookie.indexOf(name) === 0) {
      return cookie.split("=")[1];
    }
  }
  return undefined;
}

export function showCookies(Cookies) {
  const list_node = document.getElementsByTagName("ul")[0];
  let result = "";
  for (const element of Cookies) {
    result +=
      "<li><strong>" + element.name + "</strong> = " + element.value + "</li>";
  }
  list_node.innerHTML = result;
}

export function doCookie() {
  const name_node = document.getElementsByTagName("input")[0];
  const value_node = document.getElementsByTagName("input")[1];

  let name = name_node.value;
  name_node.value = "";

  let value = value_node.value;
  value_node.value = "";

  setCookie(name, value, 1);
  let cookies = listAllCookies();
  showCookies(cookies);
}

// https://stackoverflow.com/questions/3400759/how-can-i-list-all-cookies-for-the-current-page-with-javascript
export function listAllCookies() {
  const cookies = document.cookie.split(";");
  const result = [];
  for (var i = 0; i < cookies.length; i++) {
    let parts = cookies[i].split("=");
    console.log(parts);
    let cookie = {
      name: parts[0],
      value: parts[1],
    };
    result.push(cookie);
  }
 return result;
}